curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 1,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"\",
        \"value\": 1,
        \"type\": \"STEP_ONE\"
        }"

curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 1,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"\",
        \"value\": 1,
        \"type\": \"STEP_TWO\"
        }"

curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 1,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"\",
        \"value\": 1,
        \"type\": \"STEP_THREE\"
        }"

curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 2,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"\",
        \"value\": 1,
        \"type\": \"STEP_ONE\"
        }"

curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 3,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"\",
        \"value\": 1,
        \"type\": \"STEP_ONE\"
        }"

curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 3,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"\",
        \"value\": 1,
        \"type\": \"STEP_THREE\"
        }"
