DAY=`date +%Y-%m-%dT%H:%M:%S.000Z`
curl --verbose -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"2e8f7f46-f049-4ac3-b67f-6e56212661bd\",
        \"userId\": -1,
        \"eventDate\": \"$DAY\",
        \"description\": \"script\",
        \"value\": 20,
        \"type\": \"DONATION_COMPLETE\",
        \"meta\": {\"funraise_net_amount\": 20}
        }"
