curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974893\",
        \"userId\": 1,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"\",
        \"value\": 8,
        \"type\": \"ORGANIZATION_CREATED\"
        }"
