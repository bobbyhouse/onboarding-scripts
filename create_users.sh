curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 1,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"exp.0d33de2dd25f4495bc42583e31b35d27\",
        \"value\": 1,
        \"type\": \"CREATE_USER\"
        }"

curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 2,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"exp.0d33de2dd25f4495bc42583e31b35d27\",
        \"value\": 1,
        \"type\": \"CREATE_USER\"
        }"

curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"cc16205b-98d2-4f67-86be-68a312974892\",
        \"userId\": 3,
        \"eventDate\": \"2021-10-19T02:00:00.000Z\",
        \"description\": \"exp.0d33de2dd25f4495bc42583e31b35d27\",
        \"value\": 1,
        \"type\": \"CREATE_USER\"
        }"
