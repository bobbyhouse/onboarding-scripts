curl -X POST --location "http://server.hermes.remote:5000/track" \
    -H "Content-Type: application/json" \
    -d "{
        \"orgUUID\": \"2e8f7f46-f049-4ac3-b67f-6e56212661bd\",
        \"userId\": 11,
        \"eventDate\": \"2021-12-14T21:00:00.000Z\",
        \"description\": \"\",
        \"value\": 1,
        \"type\": \"GATEWAY_CREATED\"
        }"
