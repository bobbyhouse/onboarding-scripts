create extension if not exists "uuid-ossp";
create extension if not exists citext;

create or replace function sid(prefix text) returns text
as $$ select $1 || '.' || replace(uuid_generate_v4()::text,'-','') $$
language sql;

create table organization (
    id text default sid('org'),
    name text,
    ein text,
    meta JSON,
    api text,
    "created" timestamp not null default CURRENT_TIMESTAMP,
    primary key (id)
);
