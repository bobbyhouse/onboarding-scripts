package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/go-pg/pg/v10"
)

type ProPublicSearchResponse struct {
	Organizations []ProPublicaOrganization `json:"organizations"`
}

type ProPublicaOrganization struct {
	EIN  int64  `json:"ein"`
	Name string `json:"name"`
}

type CandidSearchResponse struct {
	Data CandidData `json:"data"`
}

type CandidData struct {
	Hits []CandidOrganization `json:"hits"`
}

type CandidOrganization struct {
	EIN  string `json:"ein"`
	Name string `json:"organization_name"`
}

type Organization struct {
	tableName struct{} `pg:"organization"`
	Id        string
	Name      string
	EIN       string
	Meta      string
	API       string
}

type Repo struct {
	db *pg.DB
}

func NewRepo(db *pg.DB) Repo {
	return Repo{db}
}

func Insert(repo Repo, organization Organization) (err error) {
	_, err = repo.db.Model(&organization).Insert()

	if err != nil {
		panic(err)
	}
	return err
}

func main() {
	db := pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASS"),
		Addr:     os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT"),
		Database: os.Getenv("DB_NAME"),
	})

	repo := NewRepo(db)

	// searchURI := fmt.Sprintf(
	// 	"https://projects.propublica.org/nonprofits/api/v2/search.json?q=\"%s\"",
	// 	url.PathEscape(os.Args[1]),
	// )

	// resp, err := http.Get(searchURI)

	// if err != nil {
	// 	fmt.Println(err)
	// }

	// defer resp.Body.Close()

	// responseData, err := ioutil.ReadAll(resp.Body)
	// data := string(responseData)

	// var searchResults ProPublicSearchResponse

	// err = json.Unmarshal([]byte(data), &searchResults)

	// if err != nil {
	// 	fmt.Println(err)
	// }

	// fmt.Println("Propublica")
	// fmt.Println(searchURI)
	// fmt.Println(len(searchResults.Organizations))

	// if len(searchResults.Organizations) > 0 {
	// 	proOrganization := Organization{
	// 		Name: strings.ToLower(searchResults.Organizations[0].Name),
	// 		EIN:  strings.Replace(strconv.FormatInt(searchResults.Organizations[0].EIN, 10), "-", "", -1),
	// 		Meta: data,
	// 		API:  "propublica",
	// 	}

	// 	Insert(repo, proOrganization)
	// }

	searchURI := "https://api.candid.org/essentials/v2"
	searchTerm := fmt.Sprintf("{\"search_terms\":\"%s\"}", os.Args[1])
	payload := strings.NewReader(searchTerm)
	req, _ := http.NewRequest("POST", searchURI, payload)

	req.Header.Add("Content-Type", "text/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Subscription-Key", "33a1129db4794310be589eb01dfc73bc")

	resp, err := http.DefaultClient.Do(req)

	defer resp.Body.Close()

	responseData, err := ioutil.ReadAll(resp.Body)
	data := string(responseData)

	var candidSearchResults CandidSearchResponse

	err = json.Unmarshal([]byte(data), &candidSearchResults)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Candid")
	fmt.Println(len(candidSearchResults.Data.Hits))
	fmt.Println(searchTerm)

	if len(candidSearchResults.Data.Hits) > 0 {
		candidOrganization := Organization{
			Name: strings.ToLower(candidSearchResults.Data.Hits[0].Name),
			EIN:  strings.Replace(candidSearchResults.Data.Hits[0].EIN, "-", "", -1),
			Meta: data,
			API:  "candid",
		}

		Insert(repo, candidOrganization)
	}
}
