number=`cat number`
number=$((number+1))
echo $number > number

orgname="Funraise$number"
email="bobby+$number@funraise.io"

server="localhost:9000"
admin_token=$(curl -s $server/api/v2/admin/token -u "admin:123456" | jq -r '.token')
sf_id="$number"

#FEATURES
FEATURES=(
    '"constant_contact_integration"'
    '"donor_covers_fees"'
    '"double_the_donation_integration"'
    '"gateway_paypal"'
    '"gateway_stripe"'
    '"gateway_stripe_payment_intents"'
    '"imports"'
    '"imports_transactions"'
    '"mailchimp_integration"'
    '"salesforce_integration"'
    '"stripe_aot_collection"'
    '"stripe_terminal"'
    '"attachments"'
)

features_data=$(printf ",%s" "${FEATURES[@]}")
features_data=${features_data:1}
features_data='{"features":['$features_data']}'

# HEADERS
AUTH_TOKEN="X-AUTH-TOKEN: $admin_token"
CONTENT_TYPE="Content-Type: application/json"

# APIs
ORG_API="$server/api/v1/organization"
ORG_DATA='{"sfId":"'$sf_id'","name":"'$orgname'","ein":"'$sf_id'","address":"'$sf_id' E Street Ave","city":"Townsville","state":"CA","zip":"12345","country":"US","phone":"5555555555","website":"","currency":"USD"}'

USER_API="$server/api/v1/organization/user"
USER_DATA='{"emailAddress":"'$email'","firstName":"Bobby","lastName":"House","sfOrgId":"'$sf_id'"}'

RESPONSE=`curl -v "$ORG_API" -H "$AUTH_TOKEN" -H "$CONTENT_TYPE" -d "$ORG_DATA"`
UUID=`echo $RESPONSE | jq --raw-output '.orgUUID'`

USER_RESPONSE=`curl -v "$USER_API" -H "$AUTH_TOKEN" -H "$CONTENT_TYPE" -d "$USER_DATA"`

FEATURE_API="$server/api/v1/admin/features/organizations/$UUID"
RESPONSE=`curl -v "$FEATURE_API" -H "$AUTH_TOKEN" -H "$CONTENT_TYPE" -d "$features_data"`
#echo $RESPONSE

PAYMENTS_API="$server/api/v2/admin/organization/$UUID/payment-info/deploy"
RESPONSE=`curl -v "$PAYMENTS_API" -H "$AUTH_TOKEN" -H "$CONTENT_TYPE" -d "{}"`
#echo $RESPONSE

echo ""
echo $USER_RESPONSE | jq --raw-output '.firstLoginUrl'
